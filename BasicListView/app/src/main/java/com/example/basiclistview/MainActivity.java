package com.example.basiclistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String[] Movies = new String[] {
                "Batman vs Superman",
                "13 Hours", "Deadpool",
                "The Forest ",
                "The 5th Wave",
                "10 Cloverfield Lane",
                "Free State of Jones",
                "Triple 9",
                "Hail, Caesar!",
                "Captain America"
        };

        ListView listViewMovie = findViewById(R.id.listViewMovie);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,Movies);
        listViewMovie.setAdapter(adapter);
        listViewMovie.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String selected = (String) ((TextView) view).getText();
                Toast.makeText(getApplicationContext(),"You clicked "+ selected,Toast.LENGTH_SHORT).show();
            }
        });
    }
}